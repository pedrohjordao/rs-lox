mod internals;

use log::{error, info, warn};

use internals::{
    error::LoxError, expr::Expr, interpreter::interpret, parser::parse, scanner::Scanner,
    tokens::Literal,
};
use std::io::Read;
use std::io::Write;
use std::{
    error::Error,
    io::{BufRead, BufReader},
};

fn main() {
    flexi_logger::Logger::with_env().start().unwrap();
    use std::process::exit;
    let c_args = std::env::args().collect::<Vec<String>>();
    if c_args.len() < 2 {
        run_prompt()
    } else if c_args.len() == 2 {
        match run_file(c_args.get(1).unwrap().to_string()) {
            Ok(_) => info!("Execution was a success"),
            Err(e) => {
                error!("Error executing script: {}", e);
                exit(63);
            }
        }
    }
}

fn run_file(file: String) -> Result<(), LoxError> {
    use std::fs;
    info!("Attempting to run script file: {}", file);
    let script_file = fs::File::open(file).map_err(|e| LoxError::IOError(e))?;
    let buf_reader = BufReader::new(script_file);
    buf_reader
        .lines()
        .enumerate()
        .try_for_each(|(line, maybe_line)| {
            let line = line + 1;
            maybe_line
                .map_err(|err| LoxError::IOError(err))
                .and_then(|mut l| {
                    // I don't like this, but Rust api
                    // is forcing my hand
                    l.push_str("\n");
                    run(line, l).map(|_| ())
                })
        })
}

fn run_prompt() -> ! {
    info!("Initializing prompt");
    let reader = std::io::stdin();
    let mut writer = std::io::stdout();
    let mut line = 0;
    loop {
        line += 1;
        let mut content = String::new();
        let _ = writer.write(b"\nrs-lox> ");
        let _ = writer.flush();
        let _ = reader.read_line(&mut content);
        let _ = match run(line, content) {
            Ok(expr) => writer.write(format!("{}", expr).as_bytes()),
            Err(err) => writer.write(format!("Invalid Expression: {}", err).as_bytes()),
        };
    }
}

fn run(line: usize, script_string: String) -> Result<Literal, LoxError> {
    parse(Scanner::from(line, script_string.chars())).and_then(interpret)
}
