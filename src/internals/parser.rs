use super::{
    error::LoxError,
    expr::Expr,
    tokens::{Literal, Token, TokenType},
};
use std::iter::Peekable;

pub fn parse(tokens: impl Iterator<Item = Result<Token, LoxError>>) -> Result<Expr, LoxError> {
    expression(&mut tokens.peekable())
}

pub fn synchronize<T: Iterator<Item = Result<Token, LoxError>>>(tokens: &mut Peekable<T>) {
    match tokens.peek() {
        Some(Ok(tk)) if *tk.token_type() == TokenType::Semicolon => {
            let _ = tokens.next();
            return;
        }
        _ => (),
    }

    while tokens.peek().is_some() {
        let nxt = tokens.peek().unwrap();
        match nxt {
            Ok(val) => match val.token_type() {
                TokenType::Class
                | TokenType::Fun
                | TokenType::Var
                | TokenType::For
                | TokenType::If
                | TokenType::While
                | TokenType::Print
                | TokenType::Return => return,
                _ => {
                    let _ = tokens.next();
                }
            },
            _ => {
                let _ = tokens.next();
            }
        }
    }
}

fn expression<T: Iterator<Item = Result<Token, LoxError>>>(
    tokens: &mut Peekable<T>,
) -> Result<Expr, LoxError> {
    equality(tokens)
}

fn equality<T: Iterator<Item = Result<Token, LoxError>>>(
    tokens: &mut Peekable<T>,
) -> Result<Expr, LoxError> {
    let mut left = comparisson(tokens)?;

    while let Some(Ok(nxt)) = tokens.peek() {
        if [TokenType::EqualEqual, TokenType::BangEqual].contains(nxt.token_type()) {
            let op = tokens.next().unwrap()?; // We know this is fine to unwrap because of the peek
            let right = comparisson(tokens)?;
            left = Expr::binary(Box::new(left), op, Box::new(right));
        } else {
            break;
        }
    }

    Ok(left)
}

fn comparisson<T: Iterator<Item = Result<Token, LoxError>>>(
    tokens: &mut Peekable<T>,
) -> Result<Expr, LoxError> {
    let mut left = addition(tokens)?;

    while let Some(Ok(nxt)) = tokens.peek() {
        if [
            TokenType::Greater,
            TokenType::GreaterEqual,
            TokenType::Less,
            TokenType::LessEqual,
        ]
        .contains(nxt.token_type())
        {
            let op = tokens.next().unwrap()?; // We know this is fine to unwrap because of the peek
            let right = addition(tokens)?;
            left = Expr::binary(Box::new(left), op, Box::new(right));
        } else {
            break;
        }
    }

    Ok(left)
}

fn addition<T: Iterator<Item = Result<Token, LoxError>>>(
    tokens: &mut Peekable<T>,
) -> Result<Expr, LoxError> {
    let mut left = multiplication(tokens)?;

    while let Some(Ok(nxt)) = tokens.peek() {
        if [TokenType::Minus, TokenType::Plus].contains(nxt.token_type()) {
            let op = tokens.next().unwrap()?; // We know this is fine to unwrap because of the peek
            let right = multiplication(tokens)?;
            left = Expr::binary(Box::new(left), op, Box::new(right));
        } else {
            break;
        }
    }

    Ok(left)
}

fn multiplication<T: Iterator<Item = Result<Token, LoxError>>>(
    tokens: &mut Peekable<T>,
) -> Result<Expr, LoxError> {
    let mut left = unary(tokens)?;

    while let Some(Ok(nxt)) = tokens.peek() {
        if [TokenType::Slash, TokenType::Star].contains(nxt.token_type()) {
            let op = tokens.next().unwrap()?; // We know this is fine to unwrap because of the peek
            let right = unary(tokens)?;
            left = Expr::binary(Box::new(left), op, Box::new(right));
        } else {
            break;
        }
    }

    Ok(left)
}

fn unary<T: Iterator<Item = Result<Token, LoxError>>>(
    tokens: &mut Peekable<T>,
) -> Result<Expr, LoxError> {
    if let Some(Ok(nxt)) = tokens.peek() {
        if [TokenType::Bang, TokenType::Minus].contains(nxt.token_type()) {
            let op = tokens.next().unwrap()?; // We know this is fine to unwrap because of the peek
            let right = primary(tokens)?;
            return Ok(Expr::unary(op, Box::new(right)));
        }
    }

    primary(tokens)
}

fn primary<T: Iterator<Item = Result<Token, LoxError>>>(
    tokens: &mut Peekable<T>,
) -> Result<Expr, LoxError> {
    match tokens.next() {
        Some(Ok(nxt)) => match nxt.token_type() {
            TokenType::Literal(l) => Ok(Expr::literal(l.clone())),
            TokenType::LeftParen => {
                let expr = expression(tokens)?;
                match tokens.next() {
                    Some(Ok(tk)) if *tk.token_type() == TokenType::RightParen => {
                        Ok(Expr::groupping(Box::new(expr)))
                    }
                    Some(Ok(other)) => Err(LoxError::ParseError {
                        line: other.line(),
                        location: "".to_string(),
                        message: format!("Unexpected token: {}", other.token_type()),
                    }),
                    Some(Err(err)) => Err(err),
                    None => Err(LoxError::ParseError {
                        line: 0,
                        location: "".to_string(),
                        message: "Input ended too soon".to_string(),
                    }),
                }
            }
            other => Err(LoxError::ParseError {
                location: "".to_string(),
                line: nxt.line(),
                message: format!("Unexpected token: {}", other),
            }),
        },
        Some(Err(err)) => Err(err),
        None => Err(LoxError::ParseError {
            location: "".to_string(),
            line: 0,
            message: "Unexpected end of input".to_string(),
        }),
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::io;

    #[test]
    fn simple_number_literal() {
        let input: Vec<Result<Token, LoxError>> =
            vec![Ok(Token::new(TokenType::Literal(Literal::Number(0.0)), 0))];

        let result = primary(&mut input.into_iter().peekable());

        assert!(result.is_ok());
        let unwraped = result.unwrap();
        assert_eq!(unwraped, Expr::literal(Literal::Number(0.0)));
    }

    #[test]
    fn simple_string_literal() {
        let input: Vec<Result<Token, LoxError>> = vec![Ok(Token::new(
            TokenType::Literal(Literal::StringLiteral("foo".to_owned())),
            0,
        ))];

        let result = primary(&mut input.into_iter().peekable());

        assert!(result.is_ok());
        let unwraped = result.unwrap();
        assert_eq!(
            unwraped,
            Expr::literal(Literal::StringLiteral("foo".to_owned()))
        );
    }

    #[test]
    fn simple_boolean_literal() {
        let input: Vec<Result<Token, LoxError>> = vec![
            Ok(Token::new(TokenType::Literal(Literal::Boolean(true)), 0)),
            Ok(Token::new(TokenType::Literal(Literal::Boolean(false)), 0)),
        ];

        let result = primary(&mut input.into_iter().peekable());

        assert!(result.is_ok());
        let unwraped = result.unwrap();
        assert_eq!(unwraped, Expr::literal(Literal::Boolean(true)));
    }

    #[test]
    fn groupped_single_literal() {
        let input: Vec<Result<Token, LoxError>> = vec![
            Ok(Token::new(TokenType::LeftParen, 0)),
            Ok(Token::new(TokenType::Literal(Literal::Boolean(true)), 0)),
            Ok(Token::new(TokenType::RightParen, 0)),
        ];
        let result = primary(&mut input.into_iter().peekable());

        assert!(result.is_ok());
        let unwraped = result.unwrap();
        assert_eq!(
            unwraped,
            Expr::groupping(Box::new(Expr::literal(Literal::Boolean(true))))
        );
    }

    #[test]
    fn unclosed_parens_error() {
        let input: Vec<Result<Token, LoxError>> = vec![
            Ok(Token::new(TokenType::LeftParen, 0)),
            Ok(Token::new(TokenType::Literal(Literal::Boolean(true)), 0)),
        ];
        let result = primary(&mut input.into_iter().peekable());

        assert!(result.is_err());
        let unwraped = result.unwrap_err();
        match unwraped {
            LoxError::ParseError {
                line,
                location,
                message,
            } => {
                assert_eq!(line, 0);
                assert_eq!(location, "".to_owned());
                assert_eq!(message, "Input ended too soon".to_string());
            }
            _ => assert!(false),
        }
    }

    #[test]
    fn forward_error_on_input() {
        let input: Vec<Result<Token, LoxError>> =
            vec![Err(LoxError::IOError(io::Error::from_raw_os_error(42)))];
        let result = primary(&mut input.into_iter().peekable());

        assert!(result.is_err());
        let unwraped = result.unwrap_err();
        match unwraped {
            LoxError::IOError(err) => assert_eq!(err.raw_os_error(), Some(42)),
            _ => assert!(false),
        }
    }

    #[test]
    fn unary_simple_literal() {
        let input: Vec<Result<Token, LoxError>> =
            vec![Ok(Token::new(TokenType::Literal(Literal::Number(0.0)), 0))];

        let result = unary(&mut input.into_iter().peekable());

        assert!(result.is_ok());
        let unwraped = result.unwrap();
        assert_eq!(unwraped, Expr::literal(Literal::Number(0.0)));
    }

    #[test]
    fn unary_minus() {
        let input: Vec<Result<Token, LoxError>> = vec![
            Ok(Token::new(TokenType::Minus, 0)),
            Ok(Token::new(TokenType::Literal(Literal::Number(0.0)), 0)),
        ];

        let result = unary(&mut input.into_iter().peekable());

        assert!(result.is_ok());
        let unwraped = result.unwrap();
        assert_eq!(
            unwraped,
            Expr::unary(
                Token::new(TokenType::Minus, 0),
                Box::new(Expr::literal(Literal::Number(0.0)))
            )
        );
    }

    #[test]
    fn unary_bang() {
        let input: Vec<Result<Token, LoxError>> = vec![
            Ok(Token::new(TokenType::Bang, 0)),
            Ok(Token::new(TokenType::Literal(Literal::Boolean(true)), 0)),
        ];

        let result = unary(&mut input.into_iter().peekable());

        assert!(result.is_ok());
        let unwraped = result.unwrap();
        assert_eq!(
            unwraped,
            Expr::unary(
                Token::new(TokenType::Bang, 0),
                Box::new(Expr::literal(Literal::Boolean(true)))
            )
        );
    }

    #[test]
    fn unary_operator_followed_by_error() {
        let input: Vec<Result<Token, LoxError>> = vec![
            Ok(Token::new(TokenType::Bang, 0)),
            Err(LoxError::IOError(io::Error::from_raw_os_error(42))),
        ];

        let result = unary(&mut input.into_iter().peekable());

        assert!(result.is_err());
        let unwraped = result.unwrap_err();
        match unwraped {
            LoxError::IOError(err) => assert_eq!(err.raw_os_error(), Some(42)),
            _ => assert!(false),
        }
    }

    #[test]
    fn multiplication_with_star() {
        let input: Vec<Result<Token, LoxError>> = vec![
            Ok(Token::new(TokenType::Literal(Literal::Number(0.0)), 0)),
            Ok(Token::new(TokenType::Star, 0)),
            Ok(Token::new(TokenType::Literal(Literal::Number(1.0)), 0)),
        ];

        let result = multiplication(&mut input.into_iter().peekable());

        assert!(result.is_ok());
        let unwraped = result.unwrap();
        assert_eq!(
            unwraped,
            Expr::binary(
                Box::new(Expr::literal(Literal::Number(0.0))),
                Token::new(TokenType::Star, 0),
                Box::new(Expr::literal(Literal::Number(1.0)))
            )
        );
    }

    #[test]
    fn multiplication_with_slash() {
        let input: Vec<Result<Token, LoxError>> = vec![
            Ok(Token::new(TokenType::Literal(Literal::Number(0.0)), 0)),
            Ok(Token::new(TokenType::Slash, 0)),
            Ok(Token::new(TokenType::Literal(Literal::Number(1.0)), 0)),
        ];

        let result = multiplication(&mut input.into_iter().peekable());

        assert!(result.is_ok());
        let unwraped = result.unwrap();
        assert_eq!(
            unwraped,
            Expr::binary(
                Box::new(Expr::literal(Literal::Number(0.0))),
                Token::new(TokenType::Slash, 0),
                Box::new(Expr::literal(Literal::Number(1.0)))
            )
        );
    }

    #[test]
    fn multiplication_with_left_error() {
        let input: Vec<Result<Token, LoxError>> = vec![
            Err(LoxError::IOError(io::Error::from_raw_os_error(42))),
            Ok(Token::new(TokenType::Star, 0)),
            Ok(Token::new(TokenType::Literal(Literal::Number(1.0)), 0)),
        ];

        let result = multiplication(&mut input.into_iter().peekable());

        assert!(result.is_err());
        let unwraped = result.unwrap_err();
        match unwraped {
            LoxError::IOError(err) => assert_eq!(err.raw_os_error(), Some(42)),
            _ => assert!(false),
        }
    }

    #[test]
    fn multiplication_with_right_error() {
        let input: Vec<Result<Token, LoxError>> = vec![
            Ok(Token::new(TokenType::Literal(Literal::Number(0.0)), 0)),
            Ok(Token::new(TokenType::Star, 0)),
            Err(LoxError::IOError(io::Error::from_raw_os_error(42))),
        ];

        let result = multiplication(&mut input.into_iter().peekable());

        assert!(result.is_err());
        let unwraped = result.unwrap_err();
        match unwraped {
            LoxError::IOError(err) => assert_eq!(err.raw_os_error(), Some(42)),
            _ => assert!(false),
        }
    }

    #[test]
    fn multiplication_passthrough() {
        let input: Vec<Result<Token, LoxError>> =
            vec![Ok(Token::new(TokenType::Literal(Literal::Number(0.0)), 0))];

        let result = multiplication(&mut input.into_iter().peekable());

        assert!(result.is_ok());
        let unwraped = result.unwrap();
        assert_eq!(unwraped, Expr::literal(Literal::Number(0.0)));
    }

    #[test]
    fn addition_with_minus() {
        let input: Vec<Result<Token, LoxError>> = vec![
            Ok(Token::new(TokenType::Literal(Literal::Number(0.0)), 0)),
            Ok(Token::new(TokenType::Minus, 0)),
            Ok(Token::new(TokenType::Literal(Literal::Number(1.0)), 0)),
        ];

        let result = addition(&mut input.into_iter().peekable());

        assert!(result.is_ok());
        let unwraped = result.unwrap();
        assert_eq!(
            unwraped,
            Expr::binary(
                Box::new(Expr::literal(Literal::Number(0.0))),
                Token::new(TokenType::Minus, 0),
                Box::new(Expr::literal(Literal::Number(1.0)))
            )
        );
    }

    #[test]
    fn addition_with_plus() {
        let input: Vec<Result<Token, LoxError>> = vec![
            Ok(Token::new(TokenType::Literal(Literal::Number(0.0)), 0)),
            Ok(Token::new(TokenType::Plus, 0)),
            Ok(Token::new(TokenType::Literal(Literal::Number(1.0)), 0)),
        ];

        let result = addition(&mut input.into_iter().peekable());

        assert!(result.is_ok());
        let unwraped = result.unwrap();
        assert_eq!(
            unwraped,
            Expr::binary(
                Box::new(Expr::literal(Literal::Number(0.0))),
                Token::new(TokenType::Plus, 0),
                Box::new(Expr::literal(Literal::Number(1.0)))
            )
        );
    }

    #[test]
    fn addition_with_left_error() {
        let input: Vec<Result<Token, LoxError>> = vec![
            Err(LoxError::IOError(io::Error::from_raw_os_error(42))),
            Ok(Token::new(TokenType::Plus, 0)),
            Ok(Token::new(TokenType::Literal(Literal::Number(1.0)), 0)),
        ];

        let result = addition(&mut input.into_iter().peekable());

        assert!(result.is_err());
        let unwraped = result.unwrap_err();
        match unwraped {
            LoxError::IOError(err) => assert_eq!(err.raw_os_error(), Some(42)),
            _ => assert!(false),
        }
    }

    #[test]
    fn addition_with_right_error() {
        let input: Vec<Result<Token, LoxError>> = vec![
            Ok(Token::new(TokenType::Literal(Literal::Number(1.0)), 0)),
            Ok(Token::new(TokenType::Star, 0)),
            Err(LoxError::IOError(io::Error::from_raw_os_error(42))),
        ];

        let result = addition(&mut input.into_iter().peekable());

        assert!(result.is_err());
        let unwraped = result.unwrap_err();
        match unwraped {
            LoxError::IOError(err) => assert_eq!(err.raw_os_error(), Some(42)),
            _ => assert!(false),
        }
    }

    #[test]
    fn addition_passthrough() {
        let input: Vec<Result<Token, LoxError>> =
            vec![Ok(Token::new(TokenType::Literal(Literal::Number(0.0)), 0))];

        let result = addition(&mut input.into_iter().peekable());

        assert!(result.is_ok());
        let unwraped = result.unwrap();
        assert_eq!(unwraped, Expr::literal(Literal::Number(0.0)));
    }

    #[test]
    fn comparisson_with_greater() {
        let input: Vec<Result<Token, LoxError>> = vec![
            Ok(Token::new(TokenType::Literal(Literal::Number(0.0)), 0)),
            Ok(Token::new(TokenType::Greater, 0)),
            Ok(Token::new(TokenType::Literal(Literal::Number(1.0)), 0)),
        ];

        let result = comparisson(&mut input.into_iter().peekable());

        assert!(result.is_ok());
        let unwraped = result.unwrap();
        assert_eq!(
            unwraped,
            Expr::binary(
                Box::new(Expr::literal(Literal::Number(0.0))),
                Token::new(TokenType::Greater, 0),
                Box::new(Expr::literal(Literal::Number(1.0)))
            )
        );
    }

    #[test]
    fn comparisson_with_greater_equal() {
        let input: Vec<Result<Token, LoxError>> = vec![
            Ok(Token::new(TokenType::Literal(Literal::Number(0.0)), 0)),
            Ok(Token::new(TokenType::GreaterEqual, 0)),
            Ok(Token::new(TokenType::Literal(Literal::Number(1.0)), 0)),
        ];

        let result = comparisson(&mut input.into_iter().peekable());

        assert!(result.is_ok());
        let unwraped = result.unwrap();
        assert_eq!(
            unwraped,
            Expr::binary(
                Box::new(Expr::literal(Literal::Number(0.0))),
                Token::new(TokenType::GreaterEqual, 0),
                Box::new(Expr::literal(Literal::Number(1.0)))
            )
        );
    }

    #[test]
    fn comparisson_with_less() {
        let input: Vec<Result<Token, LoxError>> = vec![
            Ok(Token::new(TokenType::Literal(Literal::Number(0.0)), 0)),
            Ok(Token::new(TokenType::Less, 0)),
            Ok(Token::new(TokenType::Literal(Literal::Number(1.0)), 0)),
        ];

        let result = comparisson(&mut input.into_iter().peekable());

        assert!(result.is_ok());
        let unwraped = result.unwrap();
        assert_eq!(
            unwraped,
            Expr::binary(
                Box::new(Expr::literal(Literal::Number(0.0))),
                Token::new(TokenType::Less, 0),
                Box::new(Expr::literal(Literal::Number(1.0)))
            )
        );
    }

    #[test]
    fn comparisson_with_less_equal() {
        let input: Vec<Result<Token, LoxError>> = vec![
            Ok(Token::new(TokenType::Literal(Literal::Number(0.0)), 0)),
            Ok(Token::new(TokenType::LessEqual, 0)),
            Ok(Token::new(TokenType::Literal(Literal::Number(1.0)), 0)),
        ];

        let result = comparisson(&mut input.into_iter().peekable());

        assert!(result.is_ok());
        let unwraped = result.unwrap();
        assert_eq!(
            unwraped,
            Expr::binary(
                Box::new(Expr::literal(Literal::Number(0.0))),
                Token::new(TokenType::LessEqual, 0),
                Box::new(Expr::literal(Literal::Number(1.0)))
            )
        );
    }

    #[test]
    fn comparisson_with_left_error() {
        let input: Vec<Result<Token, LoxError>> = vec![
            Err(LoxError::IOError(io::Error::from_raw_os_error(42))),
            Ok(Token::new(TokenType::Greater, 0)),
            Ok(Token::new(TokenType::Literal(Literal::Number(1.0)), 0)),
        ];

        let result = comparisson(&mut input.into_iter().peekable());

        assert!(result.is_err());
        let unwraped = result.unwrap_err();
        match unwraped {
            LoxError::IOError(err) => assert_eq!(err.raw_os_error(), Some(42)),
            _ => assert!(false),
        }
    }

    #[test]
    fn comparisson_with_right_error() {
        let input: Vec<Result<Token, LoxError>> = vec![
            Ok(Token::new(TokenType::Literal(Literal::Number(1.0)), 0)),
            Ok(Token::new(TokenType::Greater, 0)),
            Err(LoxError::IOError(io::Error::from_raw_os_error(42))),
        ];

        let result = comparisson(&mut input.into_iter().peekable());

        assert!(result.is_err());
        let unwraped = result.unwrap_err();
        match unwraped {
            LoxError::IOError(err) => assert_eq!(err.raw_os_error(), Some(42)),
            _ => assert!(false),
        }
    }

    #[test]
    fn comparisson_passthrough() {
        let input: Vec<Result<Token, LoxError>> =
            vec![Ok(Token::new(TokenType::Literal(Literal::Number(0.0)), 0))];

        let result = comparisson(&mut input.into_iter().peekable());

        assert!(result.is_ok());
        let unwraped = result.unwrap();
        assert_eq!(unwraped, Expr::literal(Literal::Number(0.0)));
    }

    #[test]
    fn equality_with_equal_equal() {
        let input: Vec<Result<Token, LoxError>> = vec![
            Ok(Token::new(TokenType::Literal(Literal::Number(0.0)), 0)),
            Ok(Token::new(TokenType::EqualEqual, 0)),
            Ok(Token::new(TokenType::Literal(Literal::Number(1.0)), 0)),
        ];

        let result = equality(&mut input.into_iter().peekable());

        assert!(result.is_ok());
        let unwraped = result.unwrap();
        assert_eq!(
            unwraped,
            Expr::binary(
                Box::new(Expr::literal(Literal::Number(0.0))),
                Token::new(TokenType::EqualEqual, 0),
                Box::new(Expr::literal(Literal::Number(1.0)))
            )
        );
    }

    #[test]
    fn equality_with_bang_equal() {
        let input: Vec<Result<Token, LoxError>> = vec![
            Ok(Token::new(TokenType::Literal(Literal::Number(0.0)), 0)),
            Ok(Token::new(TokenType::BangEqual, 0)),
            Ok(Token::new(TokenType::Literal(Literal::Number(1.0)), 0)),
        ];

        let result = equality(&mut input.into_iter().peekable());

        assert!(result.is_ok());
        let unwraped = result.unwrap();
        assert_eq!(
            unwraped,
            Expr::binary(
                Box::new(Expr::literal(Literal::Number(0.0))),
                Token::new(TokenType::BangEqual, 0),
                Box::new(Expr::literal(Literal::Number(1.0)))
            )
        );
    }

    #[test]
    fn equality_with_left_error() {
        let input: Vec<Result<Token, LoxError>> = vec![
            Err(LoxError::IOError(io::Error::from_raw_os_error(42))),
            Ok(Token::new(TokenType::EqualEqual, 0)),
            Ok(Token::new(TokenType::Literal(Literal::Number(1.0)), 0)),
        ];

        let result = equality(&mut input.into_iter().peekable());

        assert!(result.is_err());
        let unwraped = result.unwrap_err();
        match unwraped {
            LoxError::IOError(err) => assert_eq!(err.raw_os_error(), Some(42)),
            _ => assert!(false),
        }
    }

    #[test]
    fn equality_with_right_error() {
        let input: Vec<Result<Token, LoxError>> = vec![
            Ok(Token::new(TokenType::Literal(Literal::Number(1.0)), 0)),
            Ok(Token::new(TokenType::EqualEqual, 0)),
            Err(LoxError::IOError(io::Error::from_raw_os_error(42))),
        ];

        let result = equality(&mut input.into_iter().peekable());

        assert!(result.is_err());
        let unwraped = result.unwrap_err();
        match unwraped {
            LoxError::IOError(err) => assert_eq!(err.raw_os_error(), Some(42)),
            _ => assert!(false),
        }
    }

    #[test]
    fn equality_passthrough() {
        let input: Vec<Result<Token, LoxError>> =
            vec![Ok(Token::new(TokenType::Literal(Literal::Number(0.0)), 0))];

        let result = equality(&mut input.into_iter().peekable());

        assert!(result.is_ok());
        let unwraped = result.unwrap();
        assert_eq!(unwraped, Expr::literal(Literal::Number(0.0)));
    }

    #[test]
    fn fully_complex_expression() {
        // !(((((18.0) * 3.0) + 2.0) >= 5.0) == (((42.0) - 0.0) < ((1.0) / (- 3.0))))
        let input: Vec<Result<Token, LoxError>> = vec![
            Ok(Token::new(TokenType::Bang, 0)),
            Ok(Token::new(TokenType::LeftParen, 0)),
            Ok(Token::new(TokenType::LeftParen, 0)),
            Ok(Token::new(TokenType::LeftParen, 0)),
            Ok(Token::new(TokenType::LeftParen, 0)),
            Ok(Token::new(TokenType::LeftParen, 0)),
            Ok(Token::new(TokenType::Literal(Literal::Number(18.0)), 0)),
            Ok(Token::new(TokenType::RightParen, 0)),
            Ok(Token::new(TokenType::Star, 0)),
            Ok(Token::new(TokenType::Literal(Literal::Number(3.0)), 0)),
            Ok(Token::new(TokenType::RightParen, 0)),
            Ok(Token::new(TokenType::Plus, 0)),
            Ok(Token::new(TokenType::Literal(Literal::Number(2.0)), 0)),
            Ok(Token::new(TokenType::RightParen, 0)),
            Ok(Token::new(TokenType::GreaterEqual, 0)),
            Ok(Token::new(TokenType::Literal(Literal::Number(5.0)), 0)),
            Ok(Token::new(TokenType::RightParen, 0)),
            Ok(Token::new(TokenType::EqualEqual, 0)),
            Ok(Token::new(TokenType::LeftParen, 0)),
            Ok(Token::new(TokenType::LeftParen, 0)),
            Ok(Token::new(TokenType::LeftParen, 0)),
            Ok(Token::new(TokenType::Literal(Literal::Number(42.0)), 0)),
            Ok(Token::new(TokenType::RightParen, 0)),
            Ok(Token::new(TokenType::Minus, 0)),
            Ok(Token::new(TokenType::Literal(Literal::Number(0.0)), 0)),
            Ok(Token::new(TokenType::RightParen, 0)),
            Ok(Token::new(TokenType::Less, 0)),
            Ok(Token::new(TokenType::LeftParen, 0)),
            Ok(Token::new(TokenType::LeftParen, 0)),
            Ok(Token::new(TokenType::Literal(Literal::Number(1.0)), 0)),
            Ok(Token::new(TokenType::RightParen, 0)),
            Ok(Token::new(TokenType::Slash, 0)),
            Ok(Token::new(TokenType::LeftParen, 0)),
            Ok(Token::new(TokenType::Minus, 0)),
            Ok(Token::new(TokenType::Literal(Literal::Number(3.0)), 0)),
            Ok(Token::new(TokenType::RightParen, 0)),
            Ok(Token::new(TokenType::RightParen, 0)),
            Ok(Token::new(TokenType::RightParen, 0)),
            Ok(Token::new(TokenType::RightParen, 0)),
        ];

        let result = expression(&mut input.into_iter().peekable());

        let literal3 = Expr::literal(Literal::Number(3.0));
        let literal1 = Expr::literal(Literal::Number(1.0));
        let literal0 = Expr::literal(Literal::Number(0.0));
        let literal42 = Expr::literal(Literal::Number(42.0));
        let literal5 = Expr::literal(Literal::Number(5.0));
        let literal2 = Expr::literal(Literal::Number(2.0));
        let literal18 = Expr::literal(Literal::Number(18.0));

        let minus_three = Expr::unary(Token::new(TokenType::Minus, 0), Box::new(literal3.clone()));

        let groupping_minus_three = Expr::groupping(Box::new(minus_three));
        let groupping_one = Expr::groupping(Box::new(literal1));

        let groupping_1_slash_minus_three = Expr::groupping(Box::new(Expr::binary(
            Box::new(groupping_one),
            Token::new(TokenType::Slash, 0),
            Box::new(groupping_minus_three),
        )));

        let grouping_forthy_two = Expr::groupping(Box::new(literal42));

        let grouping_42_minus_0 = Expr::groupping(Box::new(Expr::binary(
            Box::new(grouping_forthy_two),
            Token::new(TokenType::Minus, 0),
            Box::new(literal0),
        )));

        let grouping_right = Expr::groupping(Box::new(Expr::binary(
            Box::new(grouping_42_minus_0),
            Token::new(TokenType::Less, 0),
            Box::new(groupping_1_slash_minus_three),
        )));

        let grouping_18 = Expr::groupping(Box::new(literal18));

        let grouping_18_times_3 = Expr::groupping(Box::new(Expr::binary(
            Box::new(grouping_18),
            Token::new(TokenType::Star, 0),
            Box::new(literal3),
        )));

        let grouping_18_times_3_plus_2 = Expr::groupping(Box::new(Expr::binary(
            Box::new(grouping_18_times_3),
            Token::new(TokenType::Plus, 0),
            Box::new(literal2),
        )));

        let grouping_left = Expr::groupping(Box::new(Expr::binary(
            Box::new(grouping_18_times_3_plus_2),
            Token::new(TokenType::GreaterEqual, 0),
            Box::new(literal5),
        )));

        let external_group = Expr::groupping(Box::new(Expr::binary(
            Box::new(grouping_left),
            Token::new(TokenType::EqualEqual, 0),
            Box::new(grouping_right),
        )));

        let full = Expr::unary(Token::new(TokenType::Bang, 0), Box::new(external_group));

        assert!(result.is_ok());
        let unwraped = result.unwrap();
        assert_eq!(unwraped, full);
    }
}
