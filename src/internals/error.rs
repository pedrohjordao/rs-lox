use std::{error::Error, fmt::Display};

#[derive(Debug)]
pub enum LoxError {
    IOError(std::io::Error),
    ParseError {
        line: usize,
        location: String,
        message: String,
    },
    InterpreterError {
        line: usize,
        location: String,
        message: String,
    },
}

impl Display for LoxError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            LoxError::IOError(err) => err.fmt(f),
            LoxError::ParseError {
                line,
                location,
                message,
            } => write!(f, "[line: {}] Error {}: {}", line, location, message),
            LoxError::InterpreterError {
                line,
                location,
                message,
            } => write!(f, "[line: {}] Error {}: {}", line, location, message),
        }
    }
}

impl Error for LoxError {}
