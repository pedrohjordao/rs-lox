use super::tokens::{Literal, TokenType};
use phf::phf_map;

pub static KEYWORDS: phf::Map<&'static str, TokenType> = phf_map! {
    "and" => TokenType::And,
    "class" => TokenType::Class,
    "else" => TokenType::Else,
    "false" => TokenType::Literal(Literal::Boolean(false)),
    "for" => TokenType::For,
    "fun" => TokenType::Fun,
    "if" => TokenType::If,
    "nil" => TokenType::Literal(Literal::Nil),
    "or" => TokenType::Or,
    "print" => TokenType::Print,
    "return" => TokenType::Return,
    "super" => TokenType::Super,
    "this" => TokenType::This,
    "true" => TokenType::Literal(Literal::Boolean(true)),
    "var" => TokenType::Var,
    "while" => TokenType::While,
};

#[cfg(test)]
mod tests {
    use super::KEYWORDS;
    use crate::internals::tokens::TokenType;

    #[test]
    fn is_keywords() {
        let id: &str = "for";
        assert_eq!(KEYWORDS.get(id), Some(&TokenType::For));
    }

    #[test]
    fn non_keyword() {
        let id: &str = "foo";
        assert_eq!(KEYWORDS.get(id), None);
    }
}
