use std::collections::VecDeque;

use super::{
    error::LoxError,
    keywords::KEYWORDS,
    tokens::{Literal, Token, TokenType},
};

pub struct Scanner<T: Iterator<Item = char>> {
    source: T,
    lexeme: String,
    peek_buffer: VecDeque<char>,
    line: usize,
    offset: usize,
    eof: bool,
}

impl<T: Iterator<Item = char>> Scanner<T> {
    pub fn from(line_num: usize, source: T) -> Self {
        Scanner {
            source,
            lexeme: String::new(),
            peek_buffer: VecDeque::new(),
            line: line_num,
            offset: 0,
            eof: false,
        }
    }

    fn advance(&mut self, store_lexeme: bool) -> Option<char> {
        if self.eof {
            return None;
        }

        if self.peek_buffer.len() >= 1 {
            let ret = self.peek_buffer.pop_front().unwrap();
            if store_lexeme {
                self.lexeme.push(ret);
            }
            return Some(ret);
        }

        self.source
            .next()
            .or_else(|| {
                self.eof = true;
                None
            })
            .and_then(|c| {
                if store_lexeme {
                    self.lexeme.push(c);
                }
                self.offset += 1;
                Some(c)
            })
    }

    fn peek_n(&mut self, n: usize) -> Option<char> {
        assert!(n > 0);

        while self.peek_buffer.len() < n {
            match self.source.next() {
                Some(c) => self.peek_buffer.push_back(c),
                None => break,
            }
        }

        self.peek_buffer.get(n - 1).cloned()
    }

    fn peek(&mut self) -> Option<char> {
        self.peek_n(1)
    }

    fn advance_if_match(&mut self, val: char, store_lexeme: bool) -> bool {
        match self.peek() {
            Some(c) if c == val => {
                let _ = self.advance(store_lexeme);
                true
            }
            _ => false,
        }
    }

    fn advance_if_match_fn<F>(&mut self, pred: F, store_lexeme: bool) -> bool
    where
        F: Fn(char) -> bool,
    {
        match self.peek() {
            Some(c) if pred(c) => {
                let _ = self.advance(store_lexeme);
                true
            }
            _ => false,
        }
    }

    fn advance_until_one_of(&mut self, val: &[char], store_lexeme: bool) -> Option<char> {
        assert!(val.len() > 0);

        use std::collections::HashSet;

        let values = val.iter().collect::<HashSet<_>>();

        let mut cur = self.advance(store_lexeme);
        while let Some(c) = cur {
            if values.contains(&c) {
                break;
            }
            cur = self.advance(store_lexeme);
        }

        cur
    }
}

impl<T: Iterator<Item = char>> Iterator for Scanner<T> {
    type Item = Result<Token, LoxError>;

    fn next(&mut self) -> Option<<Self as Iterator>::Item> {
        self.lexeme.clear();

        match self.advance(true) {
            None => None,
            Some(c) => match c {
                s if is_whitespace(s) => {
                    while let Some(ss) = self.peek() {
                        if is_whitespace(ss) {
                            if ss == '\n' {
                                self.line += 1;
                            }
                            self.advance(false);
                        } else {
                            break;
                        }
                    }
                    self.next()
                }
                s if is_digit(s) => {
                    while let Some(ss) = self.peek() {
                        if is_digit(ss) {
                            self.advance(true);
                        } else if ss == '.' {
                            match self.peek_n(2) {
                                Some(v) if is_digit(v) => {
                                    self.advance(true);
                                }
                                _ => {
                                    return Some(Err(LoxError::ParseError {
                                        line: self.line,
                                        location: "".to_owned(),
                                        message: "dot (.) found in number without trailling digits"
                                            .to_string(),
                                    }))
                                }
                            }
                        } else {
                            break;
                        }
                    }
                    let parsed: Result<f64, _> = self.lexeme.parse();
                    match parsed {
                        Err(parse_err) => Some(Err(LoxError::ParseError {
                            line: self.line,
                            location: "".to_owned(),
                            message: parse_err.to_string(),
                        })),
                        Ok(num) => {
                            Some(simple_token(self, TokenType::Literal(Literal::Number(num))))
                        }
                    }
                }
                s if is_alpha(s) => {
                    while let Some(ss) = self.peek() {
                        if is_alphanumeric(ss) {
                            self.advance(true);
                        } else {
                            break;
                        }
                    }
                    let identifier = &self.lexeme;
                    KEYWORDS
                        .get(identifier.as_str())
                        .cloned()
                        .map(|tt| simple_token(self, tt))
                        .or(Some(simple_token(
                            self,
                            TokenType::Literal(Literal::Identifier(identifier.clone())),
                        )))
                }
                '(' => Some(simple_token(self, TokenType::LeftParen)),
                ')' => Some(simple_token(self, TokenType::RightParen)),
                '[' => Some(simple_token(self, TokenType::LeftBrace)),
                ']' => Some(simple_token(self, TokenType::RightBrace)),
                '.' => Some(simple_token(self, TokenType::Dot)),
                ',' => Some(simple_token(self, TokenType::Comma)),
                '-' => Some(simple_token(self, TokenType::Minus)),
                '+' => Some(simple_token(self, TokenType::Plus)),
                ';' => Some(simple_token(self, TokenType::Semicolon)),
                '*' => Some(simple_token(self, TokenType::Star)),
                '!' => {
                    let tk = if self.advance_if_match('=', true) {
                        TokenType::BangEqual
                    } else {
                        TokenType::Bang
                    };
                    Some(simple_token(self, tk))
                }
                '=' => {
                    let tk = if self.advance_if_match('=', true) {
                        TokenType::EqualEqual
                    } else {
                        TokenType::Equal
                    };
                    Some(simple_token(self, tk))
                }
                '<' => {
                    let tk = if self.advance_if_match('=', true) {
                        TokenType::LessEqual
                    } else {
                        TokenType::Less
                    };
                    Some(simple_token(self, tk))
                }
                '>' => {
                    let tk = if self.advance_if_match('=', true) {
                        TokenType::GreaterEqual
                    } else {
                        TokenType::Greater
                    };
                    Some(simple_token(self, tk))
                }
                '/' => {
                    if self.advance_if_match('/', false) {
                        self.advance_until_one_of(&['\n'], false);
                        self.line += 1;
                        self.next()
                    } else if self.advance_if_match('*', false) {
                        let mut cur = self.advance_until_one_of(&['*', '\n'], false);
                        while let Some(c) = cur {
                            if c == '\n' {
                                self.line += 1;
                                cur = self.advance_until_one_of(&['*', '\n'], false);
                            } else if c == '*' && self.peek() == Some('/') {
                                // break out of the loop if we find */
                                cur = None;
                            }
                        }
                        if self.peek_n(1) == Some('/') {
                            let _ = self.advance(false);
                            self.next()
                        } else {
                            None
                        }
                    } else {
                        Some(simple_token(self, TokenType::Slash))
                    }
                }
                '"' => {
                    let mut cur = self.advance_until_one_of(&['\n', '"'], true);
                    while let Some(c) = cur {
                        if c == '\n' {
                            self.line += 1;
                            cur = self.advance_until_one_of(&['\n', '"'], true);
                        } else {
                            self.lexeme.pop();
                            self.lexeme.remove(0);
                            return Some(simple_token(
                                self,
                                TokenType::Literal(Literal::StringLiteral(self.lexeme.clone())),
                            ));
                        }
                    }
                    Some(Err(LoxError::ParseError {
                        line: self.line,
                        location: self.lexeme.clone(),
                        message: "Unterminated String".to_string(),
                    }))
                }

                unknown_token => Some(Err(LoxError::ParseError {
                    line: self.line,
                    location: "".to_owned(),
                    message: format!("Unknown token: {}", unknown_token),
                })),
            },
        }
    }
}

fn simple_token<T: Iterator<Item = char>>(
    scanner: &Scanner<T>,
    token_type: TokenType,
) -> Result<Token, LoxError> {
    full_token(scanner, token_type)
}

fn full_token<T: Iterator<Item = char>>(
    scanner: &Scanner<T>,
    token_type: TokenType,
) -> Result<Token, LoxError> {
    Ok(Token::new(token_type, scanner.line))
}

fn is_whitespace(c: char) -> bool {
    match c {
        ' ' | '\n' | '\t' | '\r' => true,
        _ => false,
    }
}

fn is_digit(c: char) -> bool {
    c >= '0' && c <= '9'
}

fn is_alpha(c: char) -> bool {
    c >= 'a' && c <= 'z' || c >= 'A' && c <= 'Z' || c == '_'
}

fn is_alphanumeric(c: char) -> bool {
    is_alpha(c) || is_digit(c)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn advance() {
        let text = "ab\nc".to_string();
        let mut scanner = Scanner::from(0, text.chars());
        assert_eq!(scanner.advance(true), Some('a'));
        assert_eq!(scanner.advance(true), Some('b'));
        assert_eq!(scanner.advance(true), Some('\n'));
        assert_eq!(scanner.advance(true), Some('c'));
        assert_eq!(scanner.advance(true), None);
    }

    #[test]
    fn peek_ahead() {
        let text = "abc\nde".to_string();
        let mut scanner = Scanner::from(0, text.chars());
        assert_eq!(scanner.peek_n(1), Some('a'));
        assert_eq!(scanner.advance(true), Some('a'));
        assert_eq!(scanner.peek_n(3), Some('\n'));
        assert_eq!(scanner.peek_n(10), None);
        assert_eq!(scanner.advance(true), Some('b'));
    }

    #[test]
    fn advance_if_match() {
        let text = "abc\nde".to_string();
        let mut scanner = Scanner::from(0, text.chars());
        assert!(scanner.advance_if_match('a', true));
        assert!(!scanner.advance_if_match('x', true));
        assert_eq!(scanner.advance(true), Some('b'));
    }

    #[test]
    fn advance_until() {
        let text = "abcdefg\n hi".to_string();
        let mut scanner = Scanner::from(0, text.chars());
        assert_eq!(scanner.advance_until_one_of(&['c'], true), Some('c'));
        assert_eq!(scanner.advance_until_one_of(&['g', 'f'], true), Some('f'));
        assert_eq!(scanner.advance_until_one_of(&['w'], true), None);
        assert_eq!(scanner.advance(true), None);
    }

    #[test]
    fn advance_until_immediate() {
        let text = "abcdefg\n hi".to_string();
        let mut scanner = Scanner::from(0, text.chars());
        assert_eq!(scanner.advance_until_one_of(&['a'], true), Some('a'));
        assert_eq!(scanner.advance(true), Some('b'));
    }

    #[test]
    fn next_when_eol() {
        let txt = "".to_string();
        let mut scanner = Scanner::from(0, txt.chars());
        match scanner.next() {
            None => assert!(true),
            Some(_) => assert!(false),
        }
    }

    #[test]
    fn white_space_only_line() {
        let txt = "   \t\r\n".to_string();
        let mut scanner = Scanner::from(0, txt.chars());
        match scanner.next() {
            None => assert!(true),
            Some(_) => assert!(false),
        }
        assert_eq!(scanner.line, 1);
    }

    #[test]
    fn triple_comment_line() {
        let txt = "/// MUCH TEXT ///".to_string();
        let mut scanner = Scanner::from(0, txt.chars());
        match scanner.next() {
            None => assert!(true),
            Some(_) => assert!(false),
        }
    }

    #[test]
    fn comment_line() {
        let txt = "// MUCH TEXT ///".to_string();
        let mut scanner = Scanner::from(0, txt.chars());
        match scanner.next() {
            None => assert!(true),
            Some(_) => assert!(false),
        }
    }

    #[test]
    fn comment_line_after_valid() {
        let txt = "* // MUCH TEXT ///".to_string();
        let scanner = Scanner::from(0, txt.chars());
        let tokens: Vec<Token> = scanner.map(|x| x).map(|x| x.unwrap()).collect();

        assert_eq!(tokens.as_slice(), [Token::new(TokenType::Star, 0)])
    }

    #[test]
    fn valid_after_comment_line() {
        let txt = "// MUCH TEXT /// \n *".to_string();
        let scanner = Scanner::from(0, txt.chars());
        let tokens: Vec<Token> = scanner.map(|x| x).map(|x| x.unwrap()).collect();

        assert_eq!(tokens.as_slice(), [Token::new(TokenType::Star, 1)]);
    }

    #[test]
    fn all_operators() {
        let txt = "* ()[].,-+;*!! !==== >< <= >= 0.0 / // MUCH TEXT ///".to_string();
        let scanner = Scanner::from(0, txt.chars());
        let tokens: Vec<Token> = scanner.map(|x| x.unwrap()).collect();

        assert_eq!(
            tokens.as_slice(),
            [
                Token::new(TokenType::Star, 0),
                Token::new(TokenType::LeftParen, 0),
                Token::new(TokenType::RightParen, 0),
                Token::new(TokenType::LeftBrace, 0),
                Token::new(TokenType::RightBrace, 0),
                Token::new(TokenType::Dot, 0),
                Token::new(TokenType::Comma, 0),
                Token::new(TokenType::Minus, 0),
                Token::new(TokenType::Plus, 0),
                Token::new(TokenType::Semicolon, 0),
                Token::new(TokenType::Star, 0),
                Token::new(TokenType::Bang, 0),
                Token::new(TokenType::Bang, 0),
                Token::new(TokenType::BangEqual, 0),
                Token::new(TokenType::EqualEqual, 0),
                Token::new(TokenType::Equal, 0),
                Token::new(TokenType::Greater, 0),
                Token::new(TokenType::Less, 0),
                Token::new(TokenType::LessEqual, 0),
                Token::new(TokenType::GreaterEqual, 0),
                Token::new(TokenType::Literal(Literal::Number(0.0)), 0),
                Token::new(TokenType::Slash, 0),
            ]
        )
    }

    #[test]
    fn block_comment() {
        let txt = "* /*asdasdasd\nasdfsdf\n*/*".to_string();
        let scanner = Scanner::from(0, txt.chars());
        let tokens: Vec<Token> = scanner.map(|x| x.unwrap()).collect();

        assert_eq!(
            tokens.as_slice(),
            [
                Token::new(TokenType::Star, 0),
                Token::new(TokenType::Star, 2),
            ]
        )
    }

    #[test]
    fn simple_string() {
        let txt = "\"TEXT\"".to_string();
        let scanner = Scanner::from(0, txt.chars());
        let tokens: Vec<Token> = scanner.map(|x| x.unwrap()).collect();

        assert_eq!(
            tokens.as_slice(),
            [Token::new(
                TokenType::Literal(Literal::StringLiteral("TEXT".to_string())),
                0
            ),]
        )
    }

    #[test]
    fn multiline_string() {
        let txt = "\"TEXT\nTEXT2\"".to_string();
        let scanner = Scanner::from(0, txt.chars());
        let tokens: Vec<Token> = scanner.map(|x| x.unwrap()).collect();

        assert_eq!(
            tokens.as_slice(),
            [Token::new(
                TokenType::Literal(Literal::StringLiteral("TEXT\nTEXT2".to_string())),
                1
            ),]
        )
    }

    #[test]
    fn one_digit_without_dot() {
        let txt = "0".to_string();
        let scanner = Scanner::from(0, txt.chars());
        let tokens: Vec<Token> = scanner.map(|x| x.unwrap()).collect();

        assert_eq!(
            tokens.as_slice(),
            [Token::new(TokenType::Literal(Literal::Number(0.0)), 0),]
        )
    }

    #[test]
    fn multi_digit_no_dot() {
        let txt = "099666".to_string();
        let scanner = Scanner::from(0, txt.chars());
        let tokens: Vec<Token> = scanner.map(|x| x.unwrap()).collect();

        assert_eq!(
            tokens.as_slice(),
            [Token::new(TokenType::Literal(Literal::Number(99666.0)), 0),]
        )
    }

    #[test]
    fn multi_digit_trailing_dot() {
        let txt = "099666. ".to_string();
        let mut scanner = Scanner::from(0, txt.chars());
        let maybe_token = scanner.next();

        let unwrapped_err = maybe_token.unwrap().unwrap_err();

        match unwrapped_err {
            LoxError::IOError(_) => assert!(false, "Should have been a parser error"),
            LoxError::ParseError {
                line,
                location,
                message,
            } => {
                assert_eq!(line, 0);
                assert_eq!(location, "".to_string());
                assert_eq!(
                    message,
                    "dot (.) found in number without trailling digits".to_string()
                )
            }
            _ => unimplemented!(),
        }
    }

    #[test]
    fn multi_digit_one_digit_after() {
        let txt = "099666.2".to_string();
        let mut scanner = Scanner::from(0, txt.chars());
        let maybe_token = scanner.next();

        let unwrapped = maybe_token.unwrap().unwrap();

        assert_eq!(
            unwrapped,
            Token::new(TokenType::Literal(Literal::Number(99666.2)), 0)
        )
    }

    #[test]
    fn multi_digit_with_dot() {
        let txt = "099.666".to_string();
        let scanner = Scanner::from(0, txt.chars());
        let tokens: Vec<Token> = scanner.map(|x| x.unwrap()).collect();

        assert_eq!(
            tokens.as_slice(),
            [Token::new(TokenType::Literal(Literal::Number(99.666)), 0),]
        )
    }

    #[test]
    fn dot_with_nothing_in_front() {
        let txt = ".666".to_string();
        let scanner = Scanner::from(0, txt.chars());
        let tokens: Vec<Token> = scanner.map(|x| x.unwrap()).collect();

        assert_eq!(
            tokens.as_slice(),
            [
                Token::new(TokenType::Dot, 0),
                Token::new(TokenType::Literal(Literal::Number(666.0)), 0),
            ]
        )
    }

    #[test]
    fn identifier_parsing() {
        let txt =
            "and class else false for fun if nil or print return super this true var while foo"
                .to_string();
        let scanner = Scanner::from(0, txt.chars());
        let tokens: Vec<Token> = scanner.map(|x| x.unwrap()).collect();

        assert_eq!(
            tokens.as_slice(),
            [
                Token::new(TokenType::And, 0),
                Token::new(TokenType::Class, 0),
                Token::new(TokenType::Else, 0),
                Token::new(TokenType::Literal(Literal::Boolean(false)), 0),
                Token::new(TokenType::For, 0),
                Token::new(TokenType::Fun, 0),
                Token::new(TokenType::If, 0),
                Token::new(TokenType::Literal(Literal::Nil), 0),
                Token::new(TokenType::Or, 0),
                Token::new(TokenType::Print, 0),
                Token::new(TokenType::Return, 0),
                Token::new(TokenType::Super, 0),
                Token::new(TokenType::This, 0),
                Token::new(TokenType::Literal(Literal::Boolean(true)), 0),
                Token::new(TokenType::Var, 0),
                Token::new(TokenType::While, 0),
                Token::new(
                    TokenType::Literal(Literal::Identifier("foo".to_string())),
                    0
                ),
            ]
        )
    }

    #[test]
    fn maximal_munch() {
        let txt = "or orpheus".to_string();
        let scanner = Scanner::from(0, txt.chars());
        let tokens: Vec<Token> = scanner.map(|x| x.unwrap()).collect();

        assert_eq!(
            tokens.as_slice(),
            [
                Token::new(TokenType::Or, 0),
                Token::new(
                    TokenType::Literal(Literal::Identifier("orpheus".to_string())),
                    0
                ),
            ]
        )
    }

    #[test]
    fn empty_string() {
        let txt = "\"\"".to_string();
        let scanner = Scanner::from(0, txt.chars());
        let tokens: Vec<Token> = scanner.map(|x| x.unwrap()).collect();

        assert_eq!(
            tokens.as_slice(),
            [Token::new(
                TokenType::Literal(Literal::StringLiteral("".to_string())),
                0
            ),]
        )
    }
}
