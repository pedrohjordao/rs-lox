use super::{error::LoxError, expr::Expr, tokens::Literal, tokens::Token, tokens::TokenType};

pub fn interpret(expr: Expr) -> Result<Literal, LoxError> {
    match expr {
        Expr::Binary { left, op, right } => binary(*left, op, *right),
        Expr::Grouping { expression } => interpret(*expression),
        Expr::Literal { literal } => Ok(literal),
        Expr::Unary { op, right } => unary(op, *right),
    }
}

fn binary(left: Expr, op: Token, right: Expr) -> Result<Literal, LoxError> {
    let left_val = interpret(left)?;
    let right_val = interpret(right)?;
    let token_type = op.token_type();

    use Literal::*;

    match (left_val, token_type, right_val) {
        (Number(l), TokenType::Less, Number(r)) => Ok(Boolean(l < r)),
        (Number(l), TokenType::LessEqual, Number(r)) => Ok(Boolean(l <= r)),
        (Number(l), TokenType::Greater, Number(r)) => Ok(Boolean(l > r)),
        (Number(l), TokenType::GreaterEqual, Number(r)) => Ok(Boolean(l >= r)),
        (Number(l), TokenType::Minus, Number(r)) => Ok(Number(l - r)),
        (Number(l), TokenType::Slash, Number(r)) => Ok(Number(l / r)),
        (Number(l), TokenType::Star, Number(r)) => Ok(Number(l * r)),
        (Number(l), TokenType::Plus, Number(r)) => Ok(Number(l + r)),
        (StringLiteral(l), TokenType::Plus, StringLiteral(r)) => {
            Ok(StringLiteral(format!("{}{}", l, r)))
        }
        (l, TokenType::EqualEqual, r) => Ok(Boolean(is_equal(l, r))),
        (l, TokenType::BangEqual, r) => Ok(Boolean(!is_equal(l, r))),
        (l, tk, r) => Err(LoxError::InterpreterError {
            line: op.line(),
            location: "".to_owned(),
            message: format!(
                "Binary token {} applied to literal of incompatible types:\n\t Left: {} {}\n\t Right: {} {}",
                tk, l.type_name(), l, r.type_name(), r
            ),
        }),
    }
}

fn is_equal(l: Literal, r: Literal) -> bool {
    match (l, r) {
        (Literal::Nil, Literal::Nil) => true,
        (Literal::Nil, _) => false,
        (a, b) => a == b,
    }
}

fn unary(token: Token, right: Expr) -> Result<Literal, LoxError> {
    let token_type = token.token_type();
    let right_val = interpret(right)?;

    match (token_type, right_val) {
        (TokenType::Minus, Literal::Number(n)) => Ok(Literal::Number(-n)),
        (TokenType::Bang, l) => Ok(Literal::Boolean(!is_truthy(l))),
        (tk, literal) => Err(LoxError::InterpreterError {
            line: token.line(),
            location: "".to_owned(),
            message: format!(
                "Unary token {} applied to literal of incompatible type {} {}",
                tk,
                literal.type_name(),
                literal
            ),
        }),
    }
}

fn is_truthy(l: Literal) -> bool {
    match l {
        Literal::Nil => false,
        Literal::Boolean(b) => b,
        _ => true,
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn literal_interpretation_returns_self_literal_number() {
        let literal = Expr::literal(Literal::Number(42.0));

        let result = interpret(literal);
        assert!(result.is_ok());
        let result2 = result.unwrap();
        assert_eq!(result2, Literal::Number(42.0))
    }

    #[test]
    fn literal_interpretation_returns_self_literal_boolean() {
        let literal = Expr::literal(Literal::Boolean(false));

        let result = interpret(literal);
        assert!(result.is_ok());
        let result2 = result.unwrap();
        assert_eq!(result2, Literal::Boolean(false))
    }

    #[test]
    fn is_truthy_is_consistent_with_spec() {
        assert!(is_truthy(Literal::Number(0.0)));
        assert!(is_truthy(Literal::Boolean(true)));
        assert!(!is_truthy(Literal::Boolean(false)));
        assert!(!is_truthy(Literal::Nil));
    }

    #[test]
    fn grouping_of_single_literal_returns_literal() {
        let groupping = Expr::groupping(Box::new(Expr::literal(Literal::Boolean(false))));

        let result = interpret(groupping);
        assert!(result.is_ok());
        let result2 = result.unwrap();
        assert_eq!(result2, Literal::Boolean(false))
    }

    #[test]
    fn groupping_of_unary_evaluates_to_interpret_of_unary() {
        let literal = Expr::groupping(Box::new(Expr::unary(
            Token::new(TokenType::Minus, 10),
            Box::new(Expr::literal(Literal::Number(42.0))),
        )));

        let result = interpret(literal);
        assert!(result.is_ok());
        let result2 = result.unwrap();
        assert_eq!(result2, Literal::Number(-42.0))
    }

    #[test]
    fn unary_minus_works_for_numbers() {
        let literal = Expr::unary(
            Token::new(TokenType::Minus, 10),
            Box::new(Expr::literal(Literal::Number(42.0))),
        );

        let result = interpret(literal);
        assert!(result.is_ok());
        let result2 = result.unwrap();
        assert_eq!(result2, Literal::Number(-42.0))
    }

    #[test]
    fn unary_minus_works_for_negative_numbers() {
        let literal = Expr::unary(
            Token::new(TokenType::Minus, 10),
            Box::new(Expr::literal(Literal::Number(-42.0))),
        );

        let result = interpret(literal);
        assert!(result.is_ok());
        let result2 = result.unwrap();
        assert_eq!(result2, Literal::Number(42.0))
    }

    #[test]
    fn unary_minus_doesnt_works_for_non_numbers() {
        let literal = Expr::unary(
            Token::new(TokenType::Minus, 10),
            Box::new(Expr::literal(Literal::Nil)),
        );

        let result = interpret(literal);
        assert!(result.is_err());

        let literal = Expr::unary(
            Token::new(TokenType::Minus, 10),
            Box::new(Expr::literal(Literal::Boolean(true))),
        );

        let result = interpret(literal);
        assert!(result.is_err());
    }

    #[test]
    fn unary_minus_of_groupping_evaluates_to_unary() {
        let literal = Expr::unary(
            Token::new(TokenType::Minus, 10),
            Box::new(Expr::groupping(Box::new(Expr::literal(Literal::Number(
                42.0,
            ))))),
        );

        let result = interpret(literal);
        assert!(result.is_ok());
        let result2 = result.unwrap();
        assert_eq!(result2, Literal::Number(-42.0))
    }

    #[test]
    fn unary_boolean_of_groupping_evaluates_to_unary() {
        let literal = Expr::unary(
            Token::new(TokenType::Bang, 10),
            Box::new(Expr::groupping(Box::new(Expr::literal(Literal::Nil)))),
        );

        let result = interpret(literal);
        assert!(result.is_ok());
        let result2 = result.unwrap();
        assert_eq!(result2, Literal::Boolean(true));

        let literal = Expr::unary(
            Token::new(TokenType::Bang, 10),
            Box::new(Expr::groupping(Box::new(Expr::literal(Literal::Boolean(
                true,
            ))))),
        );

        let result = interpret(literal);
        assert!(result.is_ok());
        let result2 = result.unwrap();
        assert_eq!(result2, Literal::Boolean(false))
    }

    #[test]
    fn binary_numeric_additions_work() {
        let literal = Expr::binary(
            Box::new(Expr::literal(Literal::Number(42.0))),
            Token::new(TokenType::Plus, 10),
            Box::new(Expr::literal(Literal::Number(42.0))),
        );

        let result = interpret(literal);
        assert!(result.is_ok());
        let result2 = result.unwrap();
        assert_eq!(result2, Literal::Number(84.0))
    }

    #[test]
    fn binary_numeric_subtraction_work() {
        let literal = Expr::binary(
            Box::new(Expr::literal(Literal::Number(42.0))),
            Token::new(TokenType::Minus, 10),
            Box::new(Expr::literal(Literal::Number(42.0))),
        );

        let result = interpret(literal);
        assert!(result.is_ok());
        let result2 = result.unwrap();
        assert_eq!(result2, Literal::Number(0.0))
    }

    #[test]
    fn binary_numeric_product_work() {
        let literal = Expr::binary(
            Box::new(Expr::literal(Literal::Number(2.0))),
            Token::new(TokenType::Star, 10),
            Box::new(Expr::literal(Literal::Number(21.0))),
        );

        let result = interpret(literal);
        assert!(result.is_ok());
        let result2 = result.unwrap();
        assert_eq!(result2, Literal::Number(42.0))
    }

    #[test]
    fn binary_numeric_division_work() {
        let literal = Expr::binary(
            Box::new(Expr::literal(Literal::Number(42.0))),
            Token::new(TokenType::Slash, 10),
            Box::new(Expr::literal(Literal::Number(2.0))),
        );

        let result = interpret(literal);
        assert!(result.is_ok());
        let result2 = result.unwrap();
        assert_eq!(result2, Literal::Number(21.0))
    }

    #[test]
    fn binary_division_by_zero_is_inf() {
        let literal = Expr::binary(
            Box::new(Expr::literal(Literal::Number(42.0))),
            Token::new(TokenType::Slash, 10),
            Box::new(Expr::literal(Literal::Number(0.0))),
        );

        let result = interpret(literal);
        assert!(result.is_ok());
        let result2 = result.unwrap();
        assert_eq!(result2, Literal::Number(f64::INFINITY))
    }

    #[test]
    fn binary_zero_divided_by_zero_is_nan() {
        let literal = Expr::binary(
            Box::new(Expr::literal(Literal::Number(0.0))),
            Token::new(TokenType::Slash, 10),
            Box::new(Expr::literal(Literal::Number(0.0))),
        );

        let result = interpret(literal);
        assert!(result.is_ok());
        let result2 = result.unwrap();
        match result2 {
            Literal::Number(x) => assert!(x.is_nan()),
            _ => assert!(false),
        }
    }

    #[test]
    fn binary_addition_of_strings_work() {
        let literal = Expr::binary(
            Box::new(Expr::literal(Literal::StringLiteral("hello".to_owned()))),
            Token::new(TokenType::Plus, 10),
            Box::new(Expr::literal(Literal::StringLiteral(" world".to_owned()))),
        );

        let result = interpret(literal);
        assert!(result.is_ok());
        let result2 = result.unwrap();
        assert_eq!(result2, Literal::StringLiteral("hello world".to_owned()))
    }

    #[test]
    fn binary_less_comparison_works() {
        let literal = Expr::binary(
            Box::new(Expr::literal(Literal::Number(0.0))),
            Token::new(TokenType::Less, 10),
            Box::new(Expr::literal(Literal::Number(42.0))),
        );

        let result = interpret(literal);
        assert!(result.is_ok());
        let result2 = result.unwrap();
        assert_eq!(result2, Literal::Boolean(true));

        let literal = Expr::binary(
            Box::new(Expr::literal(Literal::Number(42.0))),
            Token::new(TokenType::Less, 10),
            Box::new(Expr::literal(Literal::Number(42.0))),
        );

        let result = interpret(literal);
        assert!(result.is_ok());
        let result2 = result.unwrap();
        assert_eq!(result2, Literal::Boolean(false))
    }

    #[test]
    fn binary_less_equal_comparison_works() {
        let literal = Expr::binary(
            Box::new(Expr::literal(Literal::Number(0.0))),
            Token::new(TokenType::LessEqual, 10),
            Box::new(Expr::literal(Literal::Number(42.0))),
        );

        let result = interpret(literal);
        assert!(result.is_ok());
        let result2 = result.unwrap();
        assert_eq!(result2, Literal::Boolean(true));

        let literal = Expr::binary(
            Box::new(Expr::literal(Literal::Number(42.0))),
            Token::new(TokenType::LessEqual, 10),
            Box::new(Expr::literal(Literal::Number(42.0))),
        );

        let result = interpret(literal);
        assert!(result.is_ok());
        let result2 = result.unwrap();
        assert_eq!(result2, Literal::Boolean(true))
    }

    #[test]
    fn binary_greater_comparison_works() {
        let literal = Expr::binary(
            Box::new(Expr::literal(Literal::Number(0.0))),
            Token::new(TokenType::Greater, 10),
            Box::new(Expr::literal(Literal::Number(42.0))),
        );

        let result = interpret(literal);
        assert!(result.is_ok());
        let result2 = result.unwrap();
        assert_eq!(result2, Literal::Boolean(false));

        let literal = Expr::binary(
            Box::new(Expr::literal(Literal::Number(42.0))),
            Token::new(TokenType::Less, 10),
            Box::new(Expr::literal(Literal::Number(42.0))),
        );

        let result = interpret(literal);
        assert!(result.is_ok());
        let result2 = result.unwrap();
        assert_eq!(result2, Literal::Boolean(false))
    }

    #[test]
    fn binary_greater_equal_comparison_works() {
        let literal = Expr::binary(
            Box::new(Expr::literal(Literal::Number(0.0))),
            Token::new(TokenType::GreaterEqual, 10),
            Box::new(Expr::literal(Literal::Number(42.0))),
        );

        let result = interpret(literal);
        assert!(result.is_ok());
        let result2 = result.unwrap();
        assert_eq!(result2, Literal::Boolean(false));

        let literal = Expr::binary(
            Box::new(Expr::literal(Literal::Number(42.0))),
            Token::new(TokenType::LessEqual, 10),
            Box::new(Expr::literal(Literal::Number(42.0))),
        );

        let result = interpret(literal);
        assert!(result.is_ok());
        let result2 = result.unwrap();
        assert_eq!(result2, Literal::Boolean(true))
    }

    #[test]
    fn is_equal_equates_nils() {
        assert!(is_equal(Literal::Nil, Literal::Nil))
    }

    #[test]
    fn is_equal_nil_no_equal_to_not_nil() {
        assert!(!is_equal(Literal::Nil, Literal::Number(0.0)))
    }

    #[test]
    fn is_equal_defers_to_equalness_for_non_nils() {
        assert!(is_equal(Literal::Number(0.0), Literal::Number(0.0)));
        assert!(!is_equal(
            Literal::StringLiteral("Hello".to_owned()),
            Literal::StringLiteral("World".to_owned())
        ))
    }

    #[test]
    fn binary_equal_equal_comparisson_works() {
        let literal = Expr::binary(
            Box::new(Expr::literal(Literal::StringLiteral("hello".to_owned()))),
            Token::new(TokenType::EqualEqual, 10),
            Box::new(Expr::literal(Literal::StringLiteral(" world".to_owned()))),
        );

        let result = interpret(literal);
        assert!(result.is_ok());
        let result2 = result.unwrap();
        assert_eq!(result2, Literal::Boolean(false))
    }

    #[test]
    fn binary_not_equal_comparisson_works() {
        let literal = Expr::binary(
            Box::new(Expr::literal(Literal::StringLiteral("hello".to_owned()))),
            Token::new(TokenType::BangEqual, 10),
            Box::new(Expr::literal(Literal::StringLiteral(" world".to_owned()))),
        );

        let result = interpret(literal);
        assert!(result.is_ok());
        let result2 = result.unwrap();
        assert_eq!(result2, Literal::Boolean(true))
    }
}
