use std::fmt::Display;

#[derive(Debug, Clone, PartialEq)]
pub enum TokenType {
    // Single-character tokens.
    LeftParen,
    RightParen,
    LeftBrace,
    RightBrace,
    Comma,
    Dot,
    Minus,
    Plus,
    Semicolon,
    Slash,
    Star,

    // One or two character tokens.
    Bang,
    BangEqual,
    Equal,
    EqualEqual,
    Greater,
    GreaterEqual,
    Less,
    LessEqual,

    // Literals.
    Literal(Literal),

    // Keywords.
    And,
    Class,
    Else,
    Fun,
    For,
    If,
    Or,
    Print,
    Return,
    Super,
    This,
    Var,
    While,
    Eof,
}

#[derive(Debug, Clone, PartialEq)]
pub enum Literal {
    Identifier(String),
    StringLiteral(String),
    Number(f64),
    Boolean(bool),
    Nil,
}

impl Literal {
    pub fn type_name(&self) -> String {
        match self {
            Literal::Identifier(_) => todo!(),
            Literal::StringLiteral(_) => "String".to_owned(),
            Literal::Number(_) => "Number".to_owned(),
            Literal::Boolean(_) => "bool".to_owned(),
            Literal::Nil => "Nil".to_owned(),
        }
    }
}

impl Display for Literal {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Literal::Identifier(id) => write!(f, "val {} = {}", id, id), // TODO display value
            Literal::StringLiteral(lit) => write!(f, "\"{}\"", lit),
            Literal::Number(n) => write!(f, "{}", n),
            Literal::Boolean(b) => write!(f, "{}", b),
            Literal::Nil => write!(f, "nil"),
        }
    }
}

impl Eq for TokenType {}

impl TokenType {
    pub fn literal(&self) -> String {
        match self {
            TokenType::LeftParen => "(".to_owned(),
            TokenType::RightParen => ")".to_owned(),
            TokenType::LeftBrace => "[".to_owned(),
            TokenType::RightBrace => "]".to_owned(),
            TokenType::Comma => ",".to_owned(),
            TokenType::Dot => ".".to_owned(),
            TokenType::Minus => "-".to_owned(),
            TokenType::Plus => "+".to_owned(),
            TokenType::Semicolon => ";".to_owned(),
            TokenType::Slash => "/".to_owned(),
            TokenType::Star => "*".to_owned(),
            TokenType::Bang => "!".to_owned(),
            TokenType::BangEqual => "!=".to_owned(),
            TokenType::Equal => "=".to_owned(),
            TokenType::EqualEqual => "==".to_owned(),
            TokenType::Greater => ">".to_owned(),
            TokenType::GreaterEqual => ">=".to_owned(),
            TokenType::Less => "<".to_owned(),
            TokenType::LessEqual => "<=".to_owned(),
            TokenType::Literal(Literal::Identifier(id)) => id.clone(),
            TokenType::Literal(Literal::StringLiteral(lit)) => lit.clone(),
            TokenType::Literal(Literal::Number(n)) => n.to_string(),
            TokenType::Literal(Literal::Boolean(b)) => b.to_string(),
            TokenType::Literal(Literal::Nil) => "nil".to_string(),
            TokenType::And => "and".to_owned(),
            TokenType::Class => "class".to_owned(),
            TokenType::Else => "else".to_owned(),
            TokenType::Fun => "fun".to_owned(),
            TokenType::For => "for".to_owned(),
            TokenType::If => "if".to_owned(),
            TokenType::Or => "or".to_owned(),
            TokenType::Print => "print".to_owned(),
            TokenType::Return => "return".to_owned(),
            TokenType::Super => "super".to_owned(),
            TokenType::This => "this".to_owned(),
            TokenType::Var => "var".to_owned(),
            TokenType::While => "while".to_owned(),
            TokenType::Eof => "eof".to_owned(),
        }
    }
}

impl Display for TokenType {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.literal())
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Token {
    token_type: TokenType,
    line: usize,
}

impl Token {
    pub fn new(token_type: TokenType, line: usize) -> Self {
        Token { token_type, line }
    }

    pub fn line(&self) -> usize {
        self.line
    }

    pub fn token_type(&self) -> &TokenType {
        &self.token_type
    }
}
