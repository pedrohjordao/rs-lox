use super::tokens::{Literal, Token};
use std::fmt::{Display, Result};

#[derive(Debug, Clone, PartialEq)]
pub enum Expr {
    Binary {
        left: Box<Expr>,
        op: Token,
        right: Box<Expr>,
    },
    Grouping {
        expression: Box<Expr>,
    },
    Literal {
        literal: Literal,
    },
    Unary {
        op: Token,
        right: Box<Expr>,
    },
}

impl Expr {
    pub fn binary(left: Box<Expr>, op: Token, right: Box<Expr>) -> Self {
        Expr::Binary { left, op, right }
    }

    pub fn groupping(expression: Box<Expr>) -> Self {
        Expr::Grouping { expression }
    }

    pub fn literal(literal: Literal) -> Self {
        Expr::Literal { literal }
    }

    pub fn unary(op: Token, right: Box<Expr>) -> Self {
        Expr::Unary { op, right }
    }
}

impl Display for Expr {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> Result {
        match self {
            Expr::Binary { left, op, right } => {
                write!(f, "({} {} {})", op.token_type(), left, right)
            }
            Expr::Grouping { expression } => write!(f, "(group {})", expression),
            Expr::Literal { literal } => write!(f, "{}", literal),
            Expr::Unary { op, right } => write!(f, "({} {})", op.token_type(), right),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::internals::tokens::TokenType;

    #[test]
    fn fmt_pretty_literals() {
        let literal_identifier = Expr::literal(Literal::Identifier("foo".to_owned()));

        assert_eq!(format!("{}", literal_identifier), "val foo = foo");

        let string_literal_identifier =
            Expr::literal(Literal::StringLiteral("string literal".to_owned()));

        assert_eq!(
            format!("{}", string_literal_identifier),
            "\"string literal\""
        );

        let number_literal = Expr::literal(Literal::Number(0.0));

        assert_eq!(format!("{}", number_literal), "0");
    }

    #[test]
    fn fmt_pretty_binary() {
        let left = Expr::literal(Literal::Number(0.0));
        let right = Expr::literal(Literal::Number(1.0));
        let op = Token::new(TokenType::Plus, 0);

        let binary = Expr::binary(Box::new(left), op, Box::new(right));

        assert_eq!(format!("{}", binary), "(+ 0 1)")
    }

    #[test]
    fn fmt_pretty_unary() {
        let right = Expr::literal(Literal::Number(1.0));
        let op = Token::new(TokenType::Plus, 0);

        let unary = Expr::unary(op, Box::new(right));

        assert_eq!(format!("{}", unary), "(+ 1)")
    }

    #[test]
    fn fmt_pretty_groupping() {
        let left = Expr::literal(Literal::Number(0.0));
        let right = Expr::literal(Literal::Number(1.0));
        let op = Token::new(TokenType::Plus, 0);

        let binary = Expr::binary(Box::new(left), op, Box::new(right));

        let group_expr = Expr::groupping(Box::new(binary));

        assert_eq!(format!("{}", group_expr), "(group (+ 0 1))")
    }

    #[test]
    fn fmt_pretty_more_complex_sample() {
        let expression = Expr::binary(
            Box::new(Expr::unary(
                Token::new(TokenType::Minus, 0),
                Box::new(Expr::literal(Literal::Number(123.0))),
            )),
            Token::new(TokenType::Star, 0),
            Box::new(Expr::groupping(Box::new(Expr::literal(Literal::Number(
                45.67,
            ))))),
        );

        assert_eq!(format!("{}", expression), "(* (- 123) (group 45.67))")
    }
}
